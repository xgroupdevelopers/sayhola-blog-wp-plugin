��    8      �      �      �     �  !   �     �     �     �  	   �     �  .   
     9     @     I     N     V     b  	   r     |  2   �  2   �     �       
     *        I     P     \     _  <   l     �     �     �     �     �            .   (     W     g          �     �     �     �     �     �     �  /   �                     ,     0     @     R     [  @   a  �  �     8	  !   G	     i	     p	     �	     �	     �	  D   �	     �	  
   �	     
     
     
      
     7
     G
  B   [
  B   �
     �
     �
       9        V     f     x     |  K   �  #   �       "     &   >     e  "   n     �  @   �     �  #   �     #  &   5  	   \     f     �     �  	   �  
   �  F   �     �     �            
        %     E  
   L  W   W   Adult content An sms code has been sent to you. Author Blog category Blog description Blog name Blog private Blogs feature is not allowed for your package. Cancel Category Code Country Create blog Create new blog Dashboard Default language Ensure this field has no more than 100 characters. Ensure this field has no more than 200 characters. Enter your pin code Export blog Export log Length was cut automatically and exported. Logout Logout true No Phone number Please choose your country and enter your full phone number. Please enter code below. Please login Press enter to remove Press enter to select Search Select available language Select blog Select blog for import posts or create new one Select category Select default language Select language Select posts for export Settings Sign In to Hola SignIn Status Submit Subtitle The fields title, owner must make a unique set. Title Verify code Wrong code. Yes errorEmptyField errorPhoneInvalid language posts sayHola blogging export - Free Plugin for premium sayHola users. Project-Id-Version: sayHola Blogging
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-26 09:46+0000
PO-Revision-Date: 2020-08-26 13:28+0000
Last-Translator: 
Language-Team: French (France)
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Contenu adulte Un code sms vous a été envoyé. Auteur Catégorie de blog Description du blog Nom du blog Blog privé La fonctionnalité de blogs n'est pas autorisée pour votre package. Annuler Catégorie Code Pays Créer un blog Créer un nouveau blog Tableau de bord Langage par défaut Assurez-vous que ce champ ne contient pas plus de 100 caractères. Assurez-vous que ce champ ne contient pas plus de 500 caractères. Entrez votre code PIN Exporter le blog Exporter le journal La longueur a été coupée automatiquement et exportée. Se déconnecter Déconnexion vrai Non Numéro de téléphone Veuillez choisir votre pays et entrer votre numéro de téléphone complet. Veuillez saisir le code ci-dessous. Veuillez vous connecter Appuyez sur Entrée pour supprimer Appuyez sur entrée pour sélectionner Chercher Sélectionnez la langue disponible Sélectionner un blog Sélectionnez un blog pour importer des articles ou créez-en un Choisir une catégorie Sélectionnez la langue par défaut Choisir la langue Sélectionnez les articles à exporter Réglages Connectez-vous à sayHola Se connecter Statut Soumettre Sous-titre Le post avec ce titre existe déjà sur vos blogs sur sayHola Blogging Titre Vérifier le code Mauvais code. Oui Champ vide Numéro de téléphone invalide langue des postes Exportation de blogs sayHola - Plugin gratuit pour les utilisateurs premium de sayHola. 