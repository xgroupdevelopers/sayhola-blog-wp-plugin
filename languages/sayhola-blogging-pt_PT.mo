��    9      �      �      �     �  !   �     �     �     �  	          .        I     P     Y     ^     f     r  	   �     �  2   �  2   �            
   #  *   .     Y     `     l     o  <   |     �     �     �     �               ,  .   8     g     w     �     �     �     �     �     �     �     �  /   �          $     0     <     @     P  1   b     �     �  @   �  �  �     �	      �	     �	     �	     �	     �	     �	  7   �	     7
  	   @
     J
     R
     X
     f
     w
     
  C   �
  C   �
          -     B  6   [     �     �     �     �  <   �  1        :     J     g     �     �     �  ?   �                1  '   D     l     |     �     �     �     �  H   �     �               /     2     >     \     i     q  X      Adult content An sms code has been sent to you. Author Blog category Blog description Blog name Blog private Blogs feature is not allowed for your package. Cancel Category Code Country Create blog Create new blog Dashboard Default language Ensure this field has no more than 100 characters. Ensure this field has no more than 200 characters. Enter your pin code Export blog Export log Length was cut automatically and exported. Logout Logout true No Phone number Please choose your country and enter your full phone number. Please enter code below. Please login Press enter to remove Press enter to select Search Select available language Select blog Select blog for import posts or create new one Select category Select default language Select language Select posts for export Settings Sign In to Hola SignIn Status Submit Subtitle The fields title, owner must make a unique set. Title Verify code Wrong code. Yes errorEmptyField errorPhoneInvalid https://github.com/xcompanies/sayHola-blogging-WP language posts sayHola blogging export - Free Plugin for premium sayHola users. Project-Id-Version: sayHola Blogging
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-26 09:46+0000
PO-Revision-Date: 2020-08-26 13:17+0000
Last-Translator: 
Language-Team: Portuguese (Portugal)
Language: pt_PT
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Conteúdo adulto Se le ha enviado un código sms. Autor Categoría de blog Descripción del blog Nombre del blog Blog privado O recurso de blogs não é permitido para o seu pacote. Cancelar Categoria Código País Blog creativo Crear nuevo blog Tablero Idioma padrão Certifique-se de que este campo não tenha mais que 100 caracteres. Certifique-se de que este campo não tenha mais que 500 caracteres. Digite seu código PIN Blog de exportación Registro de exportación O comprimento foi cortado automaticamente e exportado. Cerrar sesión Logout verdadeiro No Número de teléfono Escolha seu país e digite seu número de telefone completo. Por favor, introduzca el código a continuación. Por favor entre Pressione Enter para remover Pressione Enter para selecionar Procurar Selecione o idioma disponível Seleccionar blog Seleccione el blog para importar publicaciones o cree uno nuevo Selecione a Categoria Selecione o idioma padrão Selecione o idioma Seleccionar publicaciones para exportar Configuraciones Inicia sesión en Hola Registrarse Estado Enviar Legenda A postagem com este título já existe em seus blogs no sayHola Blogging Título Código de verificação Código errado. Si Campo vazio Número de telefone inválido Blog privado língua publicaciones Exportación de blogs de sayHola: complemento gratuito para usuarios premium de sayHola. 