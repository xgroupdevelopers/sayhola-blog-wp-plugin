��    8      �      �      �     �  !   �     �     �     �  	   �     �  .   
     9     @     I     N     V     b  	   r     |  2   �  2   �     �       
     *        I     P     \     _  <   l     �     �     �     �     �            .   (     W     g          �     �     �     �     �     �     �  /   �                     ,     0     @     R     [  @   a  �  �     0	  #   G	     k	     q	     �	     �	     �	  5   �	  
   �	  	   �	     �	     �	     �	     
     %
     7
  I   G
  I   �
     �
     �
     	  4     	   T     ^     l     q  N     #   �     �  +     .   8     g  '   m     �  O   �     �          '  $   :     _     q  	   �     �  
   �  
   �  R   �                    ,     /     ;     S  	   [  O   e   Adult content An sms code has been sent to you. Author Blog category Blog description Blog name Blog private Blogs feature is not allowed for your package. Cancel Category Code Country Create blog Create new blog Dashboard Default language Ensure this field has no more than 100 characters. Ensure this field has no more than 200 characters. Enter your pin code Export blog Export log Length was cut automatically and exported. Logout Logout true No Phone number Please choose your country and enter your full phone number. Please enter code below. Please login Press enter to remove Press enter to select Search Select available language Select blog Select blog for import posts or create new one Select category Select default language Select language Select posts for export Settings Sign In to Hola SignIn Status Submit Subtitle The fields title, owner must make a unique set. Title Verify code Wrong code. Yes errorEmptyField errorPhoneInvalid language posts sayHola blogging export - Free Plugin for premium sayHola users. Project-Id-Version: sayHola Blogging
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-26 09:46+0000
PO-Revision-Date: 2020-08-26 13:24+0000
Last-Translator: 
Language-Team: German
Language: de_DE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Inhalt für Erwachsene Ein SMS-Code wurde an Sie gesendet. Autor Blog kategorie Blog beschreibung Blogname Blog privat Die Blog-Funktion ist für Ihr Paket nicht zulässig. Stornieren Kategorie Code Land Blog erstellen Erstelle einen neuen Blog Instrumententafel Standardsprache Stellen Sie sicher, dass dieses Feld nicht mehr als 100 Zeichen enthält. Stellen Sie sicher, dass dieses Feld nicht mehr als 500 Zeichen enthält. Geben Sie Ihren PIN-Code ein Blog exportieren Protokoll exportieren Länge wurde automatisch geschnitten und exportiert. Ausloggen Abmelden wahr Nein Telefonnummer Bitte wählen Sie Ihr Land und geben Sie Ihre vollständige Telefonnummer ein. Bitte geben Sie unten den Code ein. Bitte loggen Sie sich ein Drücken Sie zum Entfernen die Eingabetaste Drücken Sie die Eingabetaste, um auszuwählen Suche Wählen Sie die verfügbare Sprache aus Blog auswählen Wählen Sie einen Blog für den Import von Posts oder erstellen Sie einen neuen Kategorie wählen Wählen Sie die Standardsprache Sprache auswählen Beiträge für den Export auswählen die Einstellungen Melde dich bei sayHola an Einloggen Status Einreichen Untertitel Der Beitrag mit diesem Titel existiert bereits in Ihren blogs auf sayHola Blogging Titel Code überprüfen Falscher Code. Ja Leeres Feld Telefonnummer ungültig Sprache beiträge sayHola Blogging Export - Kostenloses Plugin für Premium-Benutzer von sayHola. 