��    9      �      �      �     �  !   �     �     �     �  	          .        I     P     Y     ^     f     r  	   �     �  2   �  2   �            
   #  *   .     Y     `     l     o  <   |     �     �     �     �               ,  .   8     g     w     �     �     �     �     �     �     �     �  /   �          $     0     <     @     P     b     k     q  @   �  �  �     Z	      q	     �	     �	     �	     �	     �	  8   �	     
  
    
     +
     3
     9
     G
     X
     `
  =   v
  =   �
     �
     	       5   7     m     |     �     �  :   �  1   �          4     Q     q     x     �  ?   �     �  !        #  '   8     `     p     �     �     �  
   �  K   �     �               ,     /     <     \     c  8   q  X   �   Adult content An sms code has been sent to you. Author Blog category Blog description Blog name Blog private Blogs feature is not allowed for your package. Cancel Category Code Country Create blog Create new blog Dashboard Default language Ensure this field has no more than 100 characters. Ensure this field has no more than 200 characters. Enter your pin code Export blog Export log Length was cut automatically and exported. Logout Logout true No Phone number Please choose your country and enter your full phone number. Please enter code below. Please login Press enter to remove Press enter to select Search Select available language Select blog Select blog for import posts or create new one Select category Select default language Select language Select posts for export Settings Sign In to Hola SignIn Status Submit Subtitle The fields title, owner must make a unique set. Title Verify code Wrong code. Yes errorEmptyField errorPhoneInvalid language posts sayHola Blogging sayHola blogging export - Free Plugin for premium sayHola users. Project-Id-Version: sayHola Blogging
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-26 09:46+0000
PO-Revision-Date: 2020-08-26 13:20+0000
Last-Translator: 
Language-Team: Spanish (Spain)
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Contenido para adultos Se le ha enviado un código sms. Autor Categoría de blog Descripción del blog Nombre del blog Blog privado La función de blogs no está permitida para su paquete. Cancelar Categoría Código País Blog creativo Crear nuevo blog Tablero Idioma predeterminado Asegúrese de que este campo no tenga más de 100 caracteres. Asegúrese de que este campo no tenga más de 500 caracteres. Ingrese su código pin Blog de exportación Registro de exportación La longitud se cortó automáticamente y se exportó. Cerrar sesión Cerrar sesión verdadero No Número de teléfono Elija su país e ingrese su número de teléfono completo. Por favor, introduzca el código a continuación. Por favor Iniciar sesión Presione enter para eliminar Presione enter para seleccionar Buscar Seleccionar idioma disponible Seleccionar blog Seleccione el blog para importar publicaciones o cree uno nuevo Selecciona una categoría Seleccionar idioma predeterminado Seleccione el idioma Seleccionar publicaciones para exportar Configuraciones Inicia sesión en sayHola Registrarse Estado Enviar Subtitular La publicación con este título ya existe en sus blogs en sayHola Blogging Título Código de verificación Codigo erroneo. Si Campo vacío Número de teléfono no válido idioma publicaciones La función de blogs no está permitida para su paquete. Exportación de blogs de sayHola: complemento gratuito para usuarios premium de sayHola. 