<?php

/**
 * Plugin Name: sayHola Blogging 
 * Plugin URI: https://github.com/xcompanies/sayHola-blogging-WP
 * Description: sayHola blogging export - Free Plugin for premium sayHola users.
 * Key features: Simple Install
 *               4 boxes to change 
 *               Easy API request from website
 *               Branded with your icon and store name
 *               Greetings Text	
 *               Add agents
 * Version: 1.0.11
 * Author: xDevelopers.
 * Author URI: https://xgroupcommunity.com
 * License: GPL2
 */

if ( !defined( 'ABSPATH' ) ) exit;
define('SAYHOLA_BLOGGING_FILE_PATH', __FILE__);

final class sayHolaBlogging {
    protected static $instance;
	protected $plugin_version = null;
	protected $plugin_url = null;

    protected function __construct() {
		add_action('rest_api_init', array( $this, 'sayholablogging_rest_api' ));
		add_action( 'admin_init', array( $this, 'sayholablogging_translations' ));
		add_action( 'admin_enqueue_scripts', array( $this, 'sayholablogging_load_scripts' ));
		add_action( 'admin_menu', array( $this, 'sayholablogging_admin_menu' ));
		register_activation_hook(SAYHOLA_BLOGGING_FILE_PATH, array( $this, 'sayholablogging_activation'));
		register_uninstall_hook( SAYHOLA_BLOGGING_FILE_PATH, 'sayholablogging_uninstall');
		add_filter( 'plugin_action_links', array( $this, 'sayholablogging_settings_link'), 10, 5 );
    }
	
	public function sayholablogging_activation() {
		update_option('sayholablogging_activation', 1);
	}

	public function sayholablogging_uninstall() {
		//$this->sayholablogging_reset_options();
	}

    public static function get_instance()
	{
        static $instance = false;
		if ( ! $instance ) {
			$c = __CLASS__;
			$instance = new $c;
		}
		return $instance;
	}

    public function sayholablogging_translations() {
		load_plugin_textdomain(
			'sayhola-blogging',
			false,
			'sayhola-blogging/languages'
		);
    }

	public function get_plugin_url()
	{
		if (is_null($this->plugin_url))
		{
			$this->plugin_url = plugin_dir_url( __FILE__ );
		}
		return $this->plugin_url;
	}

	public function get_plugin_version()
	{
		if (is_null($this->plugin_version))
		{
			if( !function_exists('get_plugin_data') ){
			    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			}
			$plugin_data = get_plugin_data( SAYHOLA_BLOGGING_FILE_PATH );
			$this->plugin_version = $plugin_data['Version'];
		}
		return $this->plugin_version;
	}

	function wp_get_jed_translate_data( $domain ) {
		$translations = get_translations_for_domain( $domain );

		$locale = array(
			'settings' => array(
				'domain' => $domain,
				'lang'   => is_admin() && function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale(),
				'exist'   => $translations->entries ? 'true' : 'false'
			),
		);
		if ( ! empty( $translations->headers['Plural-Forms'] ) ) {
			$locale['']['plural_forms'] = $translations->headers['Plural-Forms'];
		}
		foreach ( $translations->entries as $msgid => $entry ) {
			$locale[ $msgid ] = $entry->translations;
		}

		return $locale;
	}

	
    public function sayholablogging_load_scripts($hook)
	{
		if($hook != 'toplevel_page_sayholablogging_settings') {
			return;
		}
		if ( is_user_logged_in() ) {
			$sayholablogging = json_encode( array( 
				'root' => esc_url_raw( rest_url() ),
				'nonce' => wp_create_nonce( 'wp_rest' )
			) );
			$sayholablogging = "var sayholablogging = ${sayholablogging};";
			wp_register_script( 'captaincore-wp-api', '' );
			wp_enqueue_script( 'captaincore-wp-api' );
			wp_add_inline_script( 'captaincore-wp-api', $sayholablogging );
		}
        wp_enqueue_style('sayholablogging-settings', $this->get_plugin_url().'assets/css/app.css', false, $this->get_plugin_version());
        wp_enqueue_script('sayholablogging-vendor', $this->get_plugin_url().'assets/js/chunk-vendors.js', '', $this->get_plugin_version(), true);
		wp_enqueue_script('sayholablogging-settings', $this->get_plugin_url().'assets/js/app.js', 'sayholablogging-vendor', $hook, true);

		wp_localize_script( 
			'sayholablogging-settings',
			'sayholabloggingJSL10n',
			array(
				'translations' => $this->wp_get_jed_translate_data( 'sayhola-blogging' )
			)
		);
    }
    
    public function sayholablogging_admin_menu()
	{
		add_menu_page(
			__('sayHola Blogging', 'sayhola-blogging'),
			__('sayHola Blogging', 'sayhola-blogging'),
			'manage_options',
			'sayholablogging_settings',
			array($this, 'sayholablogging_settings_page'),
			$this->get_plugin_url().'assets/img/sayhola-icon.png'
		);
	}
	
	public function sayholablogging_set_userdata(WP_REST_Request $request) {
		$current_user = wp_get_current_user();
		if( $current_user->ID ){
			update_option('sayholablogging'.$current_user->ID.'login', serialize($request['data']));
			$core = array( 
				'data' => unserialize(get_option('sayholablogging'.$current_user->ID.'login'))
			);
			$response = new WP_REST_Response();
			$response->set_data($core);
			$response->set_status(200);
			$response = rest_ensure_response($response);
			return $response;
		} else {
			$response = new WP_REST_Response();
			$response->set_data(array('message' => __('Please login', 'sayhola-blogging')));
			$response->set_status(404);
			$response = rest_ensure_response($response);
			return $response;
		}
	}

	public function sayholablogging_get_userdata() {
		$current_user = wp_get_current_user();
		if( $current_user->ID ){
			if (!get_option('sayholablogging'.$current_user->ID.'login')) {
				$response = new WP_REST_Response();
				$response->set_data(array('message' => __('Please login', 'sayhola-blogging')));
				$response->set_status(200);
				$response = rest_ensure_response($response);
				return $response;
			}
			$core = array( 
				'data' => unserialize(get_option('sayholablogging'.$current_user->ID.'login'))
			);
			$response = new WP_REST_Response();
			$response->set_data($core);
			$response->set_status(200);
			$response = rest_ensure_response($response);
			return $response;
		} else {
			$response = new WP_REST_Response();
			$response->set_data(array('message' => __('Please login', 'sayhola-blogging')));
			$response->set_status(404);
			$response = rest_ensure_response($response);
			return $response;
		}
	}

	public function sayholablogging_get_posts() {
		$args = array(
			'post_type' => 'post',
			'posts_per_page'=>-1, 
			'numberposts'=>-1
		);
		$posts = get_posts($args);
		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		foreach( $posts as $post ) {
			$content = $post->post_content;
			$content = strip_tags($post->post_content, "<p><ul><li><div><span><strong><b><em><cite><dfn><i><big><small><font><blockquote><tt><a><u><del><s><strike><sup><sub><h1><h2><h3><h4><h5><h6><img><br>");
			$content = preg_replace('~\[[^\]]+\]~', ' ', $content);
			$content = preg_replace('/<!--(.|\s)*?-->/', '', $content);
			$content = preg_replace('/\r|\n/', '', $content);
			$content = preg_replace('/\s+/', ' ', $content);
			
			$core[] = (object) array( 
				'ID'               => $post->ID,
				'post_title'       => $post->post_title,
				'post_excerpt'     => $post->post_excerpt,
				'post_type'        => $post->post_type,
				'post_content'     => $content,
				'post_status'      => $post->post_status,
				'featured_img_url' =>  (has_post_thumbnail($post->ID)) ? get_the_post_thumbnail_url($post->ID) : null,
				'post_date'        => gmdate("Y-m-d\TH:i:s.".$micro, get_post_time('U', true, $post->ID))
			);
		}
		$response = new WP_REST_Response();
		$response->set_data($core);
		$response->set_status(200);
		$response = rest_ensure_response($response);
		return $response;
	}

	public function sayholablogging_logout() {
		$current_user = wp_get_current_user();
		if( $current_user->ID ){
			delete_option('sayholablogging'.$current_user->ID.'login', serialize($request['auth_token']));
			$response = new WP_REST_Response();
			$response->set_data(array('message' => __('Logout true', 'sayhola-blogging')));
			$response->set_status(200);
			$response = rest_ensure_response($response);
			return $response;
		} else {
			$response = new WP_REST_Response();
			$response->set_data(array('message' => __('Please login', 'sayhola-blogging')));
			$response->set_status(404);
			$response = rest_ensure_response($response);
			return $response;
		}
	}

	public function sayholablogging_rest_api() {
		register_rest_route('sayholablogging/v1','/userdata',
			array(
				'methods' => WP_REST_SERVER::READABLE,
				'callback' => array($this,'sayholablogging_get_userdata'),
				'show_in_index' => false
			)
		);
		register_rest_route('sayholablogging/v1','/userdata',
			array(
				'methods' => WP_REST_SERVER::CREATABLE,
				'callback' => array($this,'sayholablogging_set_userdata'),
				'show_in_index' => false
			)
		);
		register_rest_route('sayholablogging/v1','/posts',
			array(
				'methods' => WP_REST_SERVER::READABLE,
				'callback' => array($this,'sayholablogging_get_posts'),
				'show_in_index' => false
			)
		);
		register_rest_route('sayholablogging/v1','/logout',
			array(
				'methods' => WP_REST_SERVER::READABLE,
				'callback' => array($this,'sayholablogging_logout'),
				'show_in_index' => false
			)
		);
	}
	
    public function sayholablogging_settings_page() {
        echo '<div class="wrap"><h1>' . __("sayHola Blogging", "sayhola-blogging") . '</h1><div id="sayholablogging"></div></div>';
    }

    public function sayholablogging_settings_link( $actions, $plugin_file ) {
		static $plugin;
		if (!isset($plugin))
			$plugin = plugin_basename(SAYHOLA_BLOGGING_FILE_PATH);
		if ($plugin == $plugin_file) {
			$settings = array('settings' => '<a href="admin.php?page=sayholablogging_settings">' . __('Settings', 'sayhola-blogging') . '</a>');
			$site_link = array('support' => '<a href="https://xgroupcommunity.net" target="_blank">Support</a>');
			$actions = array_merge($settings, $actions);
			$actions = array_merge($site_link, $actions);	
		}
			return $actions;
	}
}

$sayHolaBlogging = sayHolaBlogging::get_instance();
